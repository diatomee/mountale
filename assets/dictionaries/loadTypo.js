// loadTypo returns a promise resolved when the given dictionaries are loaded
function loadTypo(aff, dic) {
	return new Promise(function(resolve, reject) {
		resolve(new Typo('fr', aff, dic, { platform: 'any' }))
	})
}
