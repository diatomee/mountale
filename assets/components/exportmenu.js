const exportmenu = {
	props: {core: Object, panel: String},
	data: function () {
		return {}
	},
	template: `<section class="menu">
			<div class="logo closeBt" @click="setPanel('editor')">X</div>
			<div class="wrapper">
				<h2>
					Code HTML<br>
					<span style="font-size:.8em;">{{core.files[0].name}}</span>
				</h2>
				<div class="options">
					<input type="checkbox" id="cb1" v-model="core.files[0].withComments" @click="toggleComments">
					<label for="cb1">Inclure les commentaires</label>
				</div>
				<textarea id="htmlCode" class="textarea" v-focus></textarea>
				<br>
				<div @click="exportODT">❖ Exporter en fODT (pour un usage dans LibreOffice Writer) (expérimental)</div>
			</div>
		</section>`,
	directives: {
		focus: {
			inserted: function (el) {
				if (!document.getElementById('htmlCode')) return
				 var mixedMode = {
					name: "htmlmixed",
					scriptTypes: [
						{
							matches: /\/x-handlebars-template|\/x-mustache/i,
							mode: null
						},
						{
							matches: /(text|application)\/(x-)?vb(a|script)/i,
							mode: "vbscript"
						}
					]
				}
				const cmInstance = CodeMirror.fromTextArea(document.getElementById('htmlCode'), {
					mode: mixedMode,
					lineWrapping: true,
					indentWithTabs: true,
					lineNumbers: true,
					viewportMargin: Infinity, //10 par défaut (nb de ligne rendues au-dessus et en-dessous de la position du curseur, "Infinity" est néfaste pour les performances et le processeur)
					cursorScrollMargin: 100,
					cursorHeight: 1, //1 par défaut, à voir selon typo
					cursorBlinkRate: 800, // 530ms par défaut
				})
				cmInstance.setValue(app.getHTML())
				cmInstance.execCommand('selectAll')
				cmInstance.focus()
				core.cm = cmInstance
			}
		}
	},
	methods: {
		exportODT: function () {
			core.saveFile(
				app.getHTML(),
				core.files[0].name + '.fodt',
				'text/xml'
			)
		},
		setPanel: function (name) {
			core.cm.toTextArea()
			this.$root.panel = name
		},
		toggleComments: function () {
			core.files[0].withComments = !core.files[0].withComments
			core.cm.setValue(this.getHTML())
			core.setLS()
		},
		getHTML: function () {
			let html = []
			const f = core.files[0]
			for (let c of f.content) html.push(f.showHTML(c))
			let res = html.join('\n\n')
			if (!f.withComments) res = res.replace(/<span class="comment"(.*?)>(.*?)<\/span>/g, '$2')
			return res
		}
	}
}
