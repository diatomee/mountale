const Cminstance = {
	props: {content: String},
	data: function () {
		return {
			
		}
	},
	template: `<textarea id="editorArea" class="editor"></textarea>`,
	mounted: function () {
		const cmInstance = CodeMirror.fromTextArea(document.getElementById('editorArea'), {
			mode: 'tale',
			theme: 'editor',
			lineWrapping: true,
			indentWithTabs: true,
			lineNumbers: true,
			viewportMargin: Infinity, //10 par défaut (nb de ligne rendues au-dessus et en-dessous de la position du curseur, "Infinity" est néfaste pour les performances et le processeur)
			//cursorScrollMargin: 100,
			spellcheck: true,
			autocorrect: true,
			cursorHeight: 1.05, //1 par défaut, à voir selon typo
			cursorBlinkRate: 1000, // 530ms par défaut
			styleActiveLine: true,
			gutters: ['CodeMirror-linenumbers', 'breakpoints']
		})
		cmInstance.setValue(core.files[0].content.join('\n\n'))
		cmInstance.focus()
		cmInstance.setCursor(cmInstance.lineCount(), 0)
		cmInstance.on('change', function () {
			core.files[0].content = cmInstance.doc.getValue().split('\n\n')
			core.setLS()
		})
		function makeMarker () {
			var marker = document.createElement('div')
			marker.style.color = '#93c6b5';
			marker.style.marginLeft = '-1px';
			marker.style.width = '3px'
			marker.style.background = '#93c6b5'
			marker.innerHTML = '|'
			return marker;
		}
		cmInstance.on('gutterClick', function(cm, n) {
			var info = cm.lineInfo(n)
			cm.setGutterMarker(n, 'breakpoints', info.gutterMarkers ? null : makeMarker())
		})
		cmInstance.on('cursorActivity', function (instance) {
			var pos = CodeMirror.Pos
			var style = instance.getTokenTypeAt(pos(instance.getCursor().line, instance.getCursor().ch))
		})

		// start spellchecking
		if (this.$root.spellActive) {
			if (!this.$root.typoLoaded) {
				this.$root.typoLoaded = loadTypo('dictionaries/fr/fr.aff', 'dictionaries/fr/fr.dic')
			}
			this.$root.typoLoaded.then(typo => startSpellCheck(cmInstance, typo))
		}

		core.cm = cmInstance
		
	},
	methods: {
		
	}
}
