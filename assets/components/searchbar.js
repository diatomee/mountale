const searchbar = {
	props: {/*core: Object, panel: String*/},
	data: function () {
		return {
			searchText: '',
			replaceText: '',
			founds: [],
			foundsSels: []
		}
	},
	directives: {
		focus: {
			inserted: function (el) {el.focus()}
		}
	},
	mounted: function () {
		if (core.cm.somethingSelected()) {
			this.searchText = core.cm.getSelections().pop()
			this.findUpdate()
		} else if (!core.cm.somethingSelected()) {
			//rien
		} else {
			this.resetFind()
		}
	},
	destroyed: function () {
		this.resetFind()
	},
	template: `<section class="findBar">
			<input
				v-model="searchText"
				v-focus
				@keydown.enter="findUpdate"
				type="text"
				title="Touche Entrée pour lancer la recherche"
				placeholder="Expression cherchée"
			/>
			<span v-if="founds.length === 0" style="font-size: .9em;line-height: 35px;margin-left: 10px;">
				Touche "Entrée" pour trouver, si trouvable.
			</span>
			<span v-else>
				<span class="toolIcon iconPrev" @click="prevOcc" title="Sélectionner l'occurence précédente"></span>
				<span class="toolIcon iconNext" @click="nextOcc" title="Sélectionner l'occurence suivante"></span>
				<span class="toolIcon iconAll" @click="allOcc" title="Sélectionner toutes les occurences"></span>
				<input
					v-model="replaceText"
					@keydown.enter="replaceSelOcc"
					type="text"
					title="Touche Entrée pour lancer la recherche"
					placeholder="Expression de substitution"
				/>
				<span v-if="foundsSels.length === 1" class="toolIcon iconSel" @click="replaceSelOcc" title="Remplacer l'occurence sélectionnée"></span>
				<span v-if="foundsSels.length > 1" class="toolIcon iconSel" @click="replaceSelOcc" :title="'Remplacer les ' + foundsSels.length + ' occurences sélectionnées'"></span>
				<span class="nbFound" v-if="founds.length > 0">{{founds.length}} occurence{{founds.length > 1 ? 's' : ''}}</span>
			</span>
		</section>`,
	methods: {
		isSelOnMark: function () {
			const sels = core.cm.getSelections()
			
		},
		currentOcc: function () {
			//présuppose qu'on a un curseur (ou une sélection) et des occurences (founds non vide)
			this.foundsSels.forEach(f => f.clear())
			this.foundsSels = []
			const getPrevSel = function (founds) {
				if (founds.length === 0) return
				const c = core.cm.getCursor()
				let mark = founds[0].sel
				for (let m of founds) {
					//le but est de se rapprocher au max du curseur, sans le dépasser, pour obtenir la marque la plus proche du curseur
					if (m.sel.head.line <= c.line && m.sel.head.ch <= c.ch) {
						mark = m.sel
					}
				}
				let index = founds.findIndex(f => f.sel === mark)
				return founds[index].sel
			}
			const prevSel = getPrevSel(this.founds)
			if (prevSel) {
				core.cm.setSelection(prevSel.anchor, prevSel.head)
				this.foundsSels.push(core.cm.markText(prevSel.anchor, prevSel.head, {className: 'highlightSelected'}))
			}
			core.cm.focus()
		},
		prevOcc: function () {
			//présuppose qu'on a un curseur (ou une sélection) et des occurences (founds non vide)
			this.foundsSels.forEach(f => f.clear())
			this.foundsSels = []
			const getPrevSel = function (founds) {
				if (founds.length === 0) return
				const c = core.cm.getCursor()
				let mark = founds[0].sel
				for (let m of founds) {
					//le but est de se rapprocher au max du curseur, sans le dépasser, pour obtenir la marque la plus proche du curseur
					if (m.sel.head.line <= c.line && m.sel.head.ch <= c.ch) {
						mark = m.sel
					}
				}
				let index = founds.findIndex(f => f.sel === mark)
				if (index > 0 && (mark.head.line === c.line && mark.head.ch === c.ch)) {
					index --
				}
				return founds[index].sel
			}
			const prevSel = getPrevSel(this.founds)
			if (prevSel) {
				core.cm.setSelection(prevSel.anchor, prevSel.head)
				this.foundsSels.push(core.cm.markText(prevSel.anchor, prevSel.head, {className: 'highlightSelected'}))
			}
			core.cm.focus()
		},
		nextOcc: function () {
			this.foundsSels.forEach(f => f.clear())
			this.foundsSels = []
			const getNextSel = function (founds) {
				if (founds.length === 0) return
				const c = core.cm.getCursor()
				let mark = founds[0].sel
				for (let m of founds) {
					//le but est de se rapprocher au max du curseur, sans le dépasser, pour obtenir la marque la plus proche du curseur
					if (m.sel.head.line <= c.line && m.sel.head.ch <= c.ch) {
						mark = m.sel
					}
				}
				let index = founds.findIndex(f => f.sel === mark)
				if (index < founds.length - 1 && (mark.head.line === c.line && mark.head.ch === c.ch)) {
					index ++
				}
				return founds[index].sel
			}
			const nextSel = getNextSel(this.founds)
			if (nextSel) {
				core.cm.setSelection(nextSel.anchor, nextSel.head)
				this.foundsSels.push(core.cm.markText(nextSel.anchor, nextSel.head, {className: 'highlightSelected'}))
			}
			core.cm.focus()
		},
		allOcc: function () {
			this.foundsSels.forEach(f => f.clear())
			this.foundsSels = []
			for (let m of this.founds) {
				this.foundsSels.push(core.cm.markText(m.sel.anchor, m.sel.head, {className: 'highlightSelected'}))
			}
			core.cm.setSelections(this.founds.map(f => f.sel))
			core.cm.focus()
		},
		replaceSelOcc: function (e) {
			console.log(this.foundsSels)
			e.preventDefault()
			core.cm.replaceSelection(this.replaceText)
			this.findUpdate()
		},
		resetFind: function () {
			this.founds.forEach(f => f.mark.clear())
			this.founds = []
			this.foundsSels = []
			core.cm.focus()
		},
		findUpdate: function (e) {
			if (e) e.preventDefault()
			this.resetFind()
			var cursor = core.cm.getSearchCursor(this.searchText)
			while (cursor.findNext()) {
				let f = cursor.from()
				let t = cursor.to()
				this.founds.push({
					mark: core.cm.markText(f, t, {className: 'highlight'}),
					sel: {
						anchor: {line: f.line, ch: f.ch},
						head: {line: t.line, ch: t.ch}
					}
				})
			}
			core.cm.focus()
			this.currentOcc()
			core.cm.focus()
		}
	}
}
