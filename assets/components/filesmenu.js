const allFiles = {
	props: {index: Number, file: Object},
	data: function () {
		return {}
	},
	template: `<div class="item">
			<input class="cb" type="checkbox" :id="'cb'+index" @click="addFileForAction(index)"/>
			<span v-if="index === 0" @click="open(file)"><b>{{file.name}}</b></span>
			<span v-else @click="open(file)">{{file.name}}</span>
		</div>`,
	updated: function () {
		for (let cb of [... document.querySelectorAll('.cb')]) cb.checked = false
	},
	methods: {
		open: function (file) {
			this.$root.panel = 'editor'
			const i = core.files.findIndex(f => f.id === file.id)
			const f = core.files[i]
			core.files.splice(i, 1)
			core.files.unshift(f)
			core.setLS()
		},
		addFileForAction: function (index) {
			if (document.getElementById('cb'+index).checked) {
				this.$parent.filesForAction.push(core.files[index])
			} else {
				const i = this.$parent.filesForAction.findIndex(f => f.id === core.files[index].id)
				this.$parent.filesForAction.splice(i, 1)
			}
		}
	}
}

const filesmenu = {
	props: {core: Object, panel: String},
	components: {allFiles},
	data: function () {
		return {
			filesForAction: []
		}
	},
	template: `<section class="menu">
			<div class="logo closeBt" @click="$root.panel = 'editor'">X</div>
			<input
				id="importer" type="file" style="position: absolute; opacity: 0; margin-top: -100px; width: 10px;"
				accept=".ml" multiple @change="importFile()"
			>
			<input
				id="importerAll" type="file" style="position: absolute; opacity: 0; margin-top: -100px; width: 10px;"
				accept=".notoj" @change="importAll()"
			>
			<div class="buttons">
				<div @click="add" class="icon2 iconAdd" title="Créer une note"></div>
				<div @click="openImport" class="icon2 iconImport" title="Importer une ou plusieurs notes"></div>
				<div @click="openImportAll" class="icon2 iconExportAll icon180" title="Importer un fichier *.notoj"></div>
				<div @click="exportAll" class="icon2 iconExportAll" title="Sauvegarder tout dans un fichier *.notoj"></div>
				<span v-if="filesForAction.length > 0">
					<div class="separator"></div>
					<div @click="remove" class="icon2 iconRm" title="Supprimer les documents sélectionnés"></div>
					<div @click="clone" class="icon2 iconClone" title="Dupliquer les documents sélectionnés"></div>
					<div @click="ml" class="icon2 iconMl" title="Exporter les documents sélectionnés"></div>
				</span>
			</div>
			<div class="wrapper" style="padding-left:80px;">
				<div class="items">
					<allFiles
						v-for="f, i of core.files"
						:key="'file' + i"
						:index="i"
						:file="f"
					></allFiles>
				</div>
			</div>
		</section>`,
	methods: {
		add: function () {
			core.addFile()
			core.setLS()
		},
		openImport: function () {
			document.getElementById('importer').click()
		},
		importFile: function () {
			// permet d'importer 1 ou plusieurs fichiers en même temps
			const files = document.getElementById('importer').files
			for (let f of files) {
				const ext = f.name.split('.').reverse()[0]
				if (ext === 'ml') {
					const reader = new FileReader()
					reader.onload = e => {
						const file = {
							name: f.name.slice(0, -3),
							content: reader.result.split('\n\n')
						}
						core.addFile(core.createFile(file))
						core.setLS()
					}
					reader.readAsText(f)
				}
			}
		},
		exportAll: function () {
			var today = new Date();
			var dd = String(today.getDate()).padStart(2, '0')
			var mm = String(today.getMonth() + 1).padStart(2, '0')
			var yyyy = today.getFullYear();
			today = dd + '-' + mm + '-' + yyyy
			core.saveFile(
				LS.getItem(LSname),
				today + '.notoj',
				'application/json'
			)
		},
		openImportAll: function () {
			document.getElementById('importerAll').click()
		},
		importAll: function () {
			const val = prompt("L'import d'un fichier *.notoj écrase toutes les données actuellement présentes. Saisir 'ok' pour confirmer.", '')
			if (val && val.trim() === 'ok') {
				const f = document.getElementById('importerAll').files[0]
				const ext = f.name.split('.').reverse()[0]
				if (ext === 'notoj') {
					const reader = new FileReader()
					reader.onload = e => {
						LS.setItem(LSname, reader.result)
						// 2. On récupère les fichiers du LS
						const files = JSON.parse(LS.getItem(LSname)).reverse()
						// 3. On réinitialise core
						core.files = []
						// 4. On fait de chaque fichier un objet "file"
						for (let f of files) core.addFile(core.createFile(f))
					}
					reader.readAsText(f)
				}
			}
		},
		
		// actions pour le fichier ouvert :

		remove: function () {
			if (!confirm('Cette action va entraîner la suppression de ' + this.filesForAction.length + ' fichier(s). Confirmer ?')) {
				return
			}
			for (let f of this.filesForAction) {
				const i = core.files.findIndex(file => file.id === f.id)
				core.files.splice(i, 1)
			}
			this.filesForAction = []
			core.setLS()
		},
		clone: function () {
			for (let f of this.filesForAction) {
				core.cloneFile(f)
			}
			this.filesForAction = []
			core.setLS()
		},
		ml: function () {
			for (let f of this.filesForAction) {
				core.saveFile(
					f.content.join('\n\n'),
					f.name + '.ml',
					'text/plain'
				)
			}
			this.filesForAction = []
		}
		
	}
}
