CodeMirror.defineMode("tale", function() {
	const inline = [ //sans : a, img, note, cmt
		'/', //italique
		'*', //gras
		'-', //suppression
		'+', //insertion
		',', //indice
		'^', //exposant
		'`', //code
		'|' //surligné
	]

	return {
		startState: function () {
			return {
				italic: false,
				strong: false,
				comment: false,
				linkImg: false,
				linkA: false,
				
				titleLvl: 0
			}
		},
		blankLine: function (state) {
			state.italic = false
			state.strong = false
			state.comment = false
			state.linkImg = false
			state.linkA = false
			state.titleLvl = 0
		},
		token: function (stream, state) {


			// On joue avec les états quand on trouve quelque chose

			if (!state.italic && stream.peek() === '/') {
				state.italic = true
				stream.next()
			}
			if (!state.strong && stream.peek() === '*') {
				state.strong = true
				stream.next()
			}
			if (!state.comment && stream.peek() === '‹' && stream.next() === '‹') {
				if (stream.skipTo('|')) {
					state.comment = true
					stream.next()
					return null
				}
			}
			if (!state.linkImg && stream.peek() === '{' && stream.next() === '{') {
				if (stream.skipTo('|')) {
					state.linkImg = true
					stream.next()
					return null
				}
			}
			if (!state.linkA && stream.peek() === '[' && stream.next() === '[') {
				if (stream.skipTo('|')) {
					state.linkA = true
					stream.next()
					return null
				}
			}
			if (stream.sol() && stream.peek() === '=') {
				while (stream.peek() === '=') {
					state.titleLvl ++
					stream.next()
				}
			}


			// Quand les états correspondent à quelque chose, on fait quelque chose

			if (state.italic) {
				if (stream.next() === '/') {
					state.italic = false
					return state.titleLvl > 0 ? 'noReadable title' : 'noReadable'
				} else {
					state.italic = false
					stream.next()
					return  state.titleLvl > 0 ? 'title' : null
				}
			}
			if (state.strong) {
				if (stream.next() === '*') {
					state.strong = false
					return state.titleLvl > 0 ? 'noReadable title' : 'noReadable'
				} else {
					state.strong = false
					stream.next()
					return  state.titleLvl > 0 ? 'title' : null
				}
			}
			if (state.comment) {
				if (stream.skipTo('›')) {
					state.comment = false
					return 'commentTxt'
				}
			}
			if (state.linkImg) {
				if (stream.skipTo('}')) {
					state.linkImg = false
					return 'linkImg'
				}
			}
			if (state.linkA) {
				if (stream.skipTo(']')) {
					state.linkA = false
					return 'linkA'
				}
			}
			if (state.titleLvl > 0 && state.titleLvl < 7) {
				stream.next()
				return 'title'
			} else {
				state.titleLvl = 0
			}


			// Une fois que tout est fait, on passe à la suite
			
			stream.next()
		}
	};
})

// Je suis un **nuk**.
