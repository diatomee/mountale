CodeMirror.defineMode("tale", function() {
	return {
		startState: function () {
			return {
				titleLvl: 0, // pour obtenir le niveau d'un titre
				isComment: false,
				isItalic: false
			}
		},
		blankLine: function (state) {
			//est appelé chaque fois qu'on tombe sur une ligne vide
			//https://stackoverflow.com/a/26460172
		},
		token: function (stream, state) {
			//L'objet 'stream' encapsule une ligne de code (les jetons ne peuvent jamais s'étendre sur des lignes) et notre position actuelle dans cette ligne.
			
			//Il faut faire avancer le flux pour traiter tout le contenu (avec stream.next() ou stream.skipTo(str) ou stream.skipToEnd(), ...

			function getComments (stream, state) {
				if (!state.isComment && stream.peek() === '‹') {
					if (stream.next() === '‹') {
						if (stream.skipTo('|')) {
							stream.next()
							state.isComment = true
							return null
						} else {
							stream.next()
						}
					}
				}
				if(state.isComment) {
					if (stream.skipTo('›')) {
						state.isComment = false
						return 'commentTxt'
					} else {
						stream.next()
					}
				} else {
					stream.skipTo('‹') || stream.next()
					//return null
				}
				
			}


			/*if (stream.sol()) {
				// un titre est une série de 1 à 6 "=" suivie d'au moins un caractère (espace compris)
				if (stream.peek() == '=') {
					state.titleLvl = 1
					stream.next()
					while (stream.peek() == '=') {
						if (state.titleLvl === 6) { //pas plus de 6 =
							state.titleLvl = 0
							stream.skipToEnd() //ici on va à la fin de la ligne sans même regarder s'il y a des marque inline dedans !!!
							return null
						}
						state.titleLvl ++
						stream.next()
					}
					if (stream.eol()) return null
					stream.skipToEnd() //ici on va à la fin de la ligne sans même regarder s'il y a des marque inline dedans !!!
					let cl = 'title' + state.titleLvl
					state.titleLvl = 0
					return cl
				}
			}*/
			if (stream.sol()) {
				// un titre est une série de 1 à 6 "=" suivie d'au moins un caractère (espace compris)
				if (stream.peek() == '=') {
					state.titleLvl = 1
					stream.next()
					while (stream.peek() == '=') {
						if (state.titleLvl === 6) { //pas plus de 6 =
							state.titleLvl = 0
							stream.skipToEnd() //ici on va à la fin de la ligne sans même regarder s'il y a des marque inline dedans !!!
							return null
						}
						state.titleLvl ++
						stream.next()
					}
					if (stream.eol()) return null
					stream.skipToEnd() //ici on va à la fin de la ligne sans même regarder s'il y a des marque inline dedans !!!
					let cl = 'title' + state.titleLvl
					state.titleLvl = 0
					return cl
				}
			}
			
			return getComments(stream, state)
		}
	};
})
