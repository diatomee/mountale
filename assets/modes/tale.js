CodeMirror.defineMode('tale', function() {
	const inline = [
		{sym: '*', style: 'strong'},
		{sym: '-', style: 'del'},
		{sym: '+', style: 'ins'},
		{sym: ',', style: 'sub'},
		{sym: '^', style: 'sup'},
		{sym: '`', style: 'code'},
		{sym: '|', style: 'mark'}
	]
	const inlineOC = [
		{o: '(', c: ')', style: 'note'}, //note
		{o: '[', c: ']', style: 'linkA'}, //lien
		{o: '{', c: '}', style: 'linkImg'}, //image
		{o: '‹', c: '›', style: 'commentTxt'}, //commentaire
	]

	return {
		startState: function () {
			return {
				mark: false, sym: undefined,
				markOC: false, symOC: undefined,
				comment: false,
				linkImg: false,
				linkA: false,
				
				titleLvl: 0
			}
		},
		blankLine: function (state) {
			state.mark = false
			state.sym = undefined
			state.markOC = false
			state.symOC = undefined
			state.comment = false
			state.linkImg = false
			state.linkA = false
			state.titleLvl = 0
		},
		token: function (stream, state) {

			if (stream.sol() && stream.peek() === '=') {
				while (stream.peek() === '=') {
					state.titleLvl ++
					stream.next()
				}
			}
			if (stream.sol() && stream.match(/\-\-\-$/)) {return 'jump'}

			//dans la regex, après (.*?), il faudrait vérifier qu'il n'y ai pas d'espace. JS ne permet pas encore de faire un negative lookbehind. Quand il le pourra, il faudra ajouter (?<!\s) après (.*?)
			for (let i of inline) {
				const rgx = new RegExp(`\\${i.sym}{2}(?!\\s)(.*?)\\${i.sym}{2}`)
				if (stream.match(rgx)) {
					return state.titleLvl > 0 ? 'title ' + i.style : i.style
				}
			}
			//if (stream.match(/\/\/(?!\s)(.*?)\/\//)) {return state.titleLvl > 0 ? 'title em' : 'em'}
			if (stream.match(/\/\/(?!\s)(.*?)\/\//, false)) {
				const matches = stream.match(/(\/\/)(?!\s)(.*?)(\/\/)/)
				matches.shift()
				return state.titleLvl > 0 ? 'title em' : 'em'
			}
			for (let i of inlineOC) {
				const rgx = new RegExp(`\\${i.o}{2}(?!\\s)(.*?)\\|(.*?)\\${i.c}{2}`)
				if (stream.match(rgx)) {
					return state.titleLvl > 0 ? 'title ' + i.style : i.style
				}
			}
			
			
			if (state.titleLvl > 0 && state.titleLvl < 7) {
				stream.next()
				return 'title'
			} else {
				state.titleLvl = 0
			}
			
			
			stream.next()
			
		}
	};
})

// Je suis un **nuk**.
