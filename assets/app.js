const LS = localStorage
const LSname = 'mountale'
const version = '0.8.0'
const core = {
	cm: undefined, //instance de CodeMirror
	caretPos: undefined, //position du caret start utile au retour arrière pour replacer le caret au bon endroit
	dict: [],
	files: [],
	addFile: (file = core.createFile()) => {
		core.files.unshift(file)
		return file
	},
	createFile: file => {
		const getUUID = () => { //not a true UUID, but light
			let uuid = "", i, rdm
			for (i = 0; i < 32; i++) {
				rdm = Math.random() * 16 | 0
				if (i == 8 || i == 12 || i == 16 || i == 20) uuid += '-'
				uuid += (i == 12 ? 4 : (i == 16 ? (rdm & 3 | 8) : rdm)).toString(16)
			}
			return uuid
		}
		const getName = () => {
			const syll = [
				'a', 'i', 'u', 'e', 'o', 'ka', 'ki', 'ku', 'ke', 'ko',
				'sa', 'shi', 'su', 'se', 'so', 'ta', 'chi', 'tsu', 'te', 'to',
				'na', 'ni', 'nu', 'ne', 'no', 'ha', 'hi', 'fu', 'he', 'ho',
				'ma', 'mi', 'mu', 'me', 'mo', 'ya', 'yu', 'yo',
				'ra', 'ri', 'ru', 're', 'ro', 'wa', 'wo', 'n'
			]
			let i = Math.floor(Math.random() * 3) + 2
			let word = ''
			while (i >= 0) {
				i--
				word += syll[Math.floor(Math.random() * syll.length)]
			}
			word = word.replace(/^n*/, '')
			if (word === '') word = 'yume'
			return word.replace(/^n*/, '')
		}
		const id = (file && file.id) ? file.id : getUUID()
		const name = (file && file.name) ? file.name : getName()
		const content = (file && file.content) ? file.content : ['Go !']
		const withComments = (file && file.withComments) ? file.withComments : false //html avec ou sans commentaires, pour le panneau html
		return {
			// props
			id, name, content, withComments,
			// methods
			showHTML: function (content) { //content est un texte
				const convertInline = text => {
					const
						markers = [
							{mark: '/', html: 'em'},
							{mark: '*', html: 'strong'},
							{mark: '|', html: 'mark'},
							{mark: '^', html: 'sup'},
							{mark: ',', html: 'sub'},
							{mark: '`', html: 'code'},
							{mark: '+', html: 'ins'},
							{mark: '-', html: 'del'}
						],
						ocMarkers = [
							{o: '{', c: '}'},
							{o: '[', c: ']'},
							{o: '‹', c: '›'}
						],
						quotes = [
							{type: "'", o: '‹', c: '›'},
							{type: '"', o: '«', c: '»'}
						]
					let newText = ` ${text} `

					// Les liens peuvent contenir // qui peut être pris pour de l'italique s'il y a plusieurs liens ou de l'italique dans le même bloc. De ce fait, il faut convertir les // des liens temporairement.
					const replacer = '!slαshes!'
					newText = newText.replace(/([a-zA-Z]:)\/\//g, '$1' + replacer)

					for (let m of markers) {
						const rgx = new RegExp(`\\${m.mark}{2}(?!\\s)(.*?)([^\'][^\\s]*?)\\${m.mark}{2}`, 'g')
						newText = newText.replace(rgx, `<${m.html}>$1$2</${m.html}>`)
					}

					// On remet bien les slashes dans les liens
					newText = newText.replace(new RegExp(`\\${replacer}`, 'g'), '//')

					for (let m of ocMarkers) {
						const rgx = new RegExp(`\\${m.o}{2}(?!\\s)(.*?)\\${m.c}{2}`, 'g')
						let match
						while ((match = rgx.exec(newText)) !== null) {
							const
								a = match,
								r = m.o + m.o + a[1].trim() + m.c + m.c,
								attrs = a[1].split('|')
							if (attrs.length === 2) {
								const url = attrs[1], txt = attrs[0]
								if (m.o === '[') {
									newText = newText.replace(r, `<a href="${url}">${txt}</a>`)
								} else if (m.o === '{') {
									newText = newText.replace(r, `<img src="${url}" alt="${txt}">`)
								} else if (m.o === '‹') {
									newText = newText.replace(r, `<span class="comment" data-comment="${url}">${txt}</span>`)
								}
							}
						}
					}
					for (let q of quotes) {
						// les deux lignes commentées permettent de mettre les doubles chevrons même quand il n'y a pas d'espace qui précède, mais empèche d'afficher l'apostrophe typographique comme il faut.
						const
							t = q.type, o = q.o, c = q.c,
							rgx = new RegExp(`(\\s)\\${t}\\s*(.*?)\\s*\\${t}`, 'g')
							//rgx = new RegExp(`\\${t}\\s*(.*?)\\s*\\${t}`, 'g')
						newText = newText.replace(rgx, `$1${o}&nbsp;$2&nbsp;${c}`)
						//newText = newText.replace(rgx, `${o}&nbsp;$1&nbsp;${c}`)
					}
					
					//on autorise que ( précède ", pour faire une conversion en «
					newText = newText.replace(new RegExp(`([\\(]){1,}\\"\\s*(.*?)\\s*\\"`, 'g'), `$1«&nbsp;$2&nbsp;»`) 
					
					//tiret d'incise (demi-cadration)
					newText = newText.replace(new RegExp(`(-)(?=[\\s\\,]+[a-zA-Z])`, 'g'), `–`)
					
					
					return newText
						.replace(/([^\s\\])\'/g, "$1’") // apostrophe
						.replace(/([^\s\\])\.{3}/g, "$1…") // points de suspension
						.replace(/\s{2,}/g, " ")
						.trim()
						.replace(/\s([;!?]){1}/g, '<span class=nnbsp>&nbsp;</span>$1') //nnbsp = espace fine insécable
						.replace(/\s([:%]){1}/g, '&nbsp;$1')
						.replace(/–\s(.*?)\s–/g, '–&nbsp;$1&nbsp;–')
						.replace(/\\(.){1}/g, '$1')
				}
				const getHTML = (tag, content, className) => {
					const
						elt = document.createElement(tag),
						tmp = document.createElement('DIV')
					elt.innerHTML = content
					if (className) elt.classList.add(className)
					tmp.appendChild(elt)
					return tmp.innerHTML
				}
				const p = lines => {
					if (lines.length > 1) {
						return getHTML('p', lines.map(l => convertInline(l)).join('<br>'), 'poem')
					}
					return getHTML('p', convertInline(lines[0]))
				}
				const h = (lines, lvl) => getHTML('h' + lvl, lines.map(l => convertInline(l)).join('<br>'))
				const figure = (lines, caption) => {
					let captionText = ''
					if (caption !== '') {
						lines.splice(0, 1)
						captionText = '<figcaption>' + convertInline(caption) + '</figcaption>'
					}
					return getHTML('figure', lines.map(l => convertInline(l)).join('<br>') + captionText)
				}
				const blockquote = (lines, caption) => {
					let captionText = ''
					if (caption !== '') {
						lines.splice(0, 1)
						captionText = '<footer>— <cite>' + convertInline(caption) + '</cite></footer>'
					}
					return getHTML('blockquote', lines.map(l => convertInline(l)).join('<br>') + captionText)
				}
				const discuss = lines => {
					lines = lines.map((l, i) => {
						const nb = lines.length
						if (nb > 1) {
							return {
								text: l.substring(1).trim(),
								classname: i === 0 ? 'discA' : (i != nb - 1 ? 'discI' : 'discZ')
							}
						}
						return {text: l.substring(1).trim()}
					})
					let html = []
					for (let l of lines) {
						html.push(getHTML('p', '— ' + convertInline(l.text), l.classname))
					}
					return html.join('')
				}
				const li = lines => {
					function itemInfos(item) {
						const uOrO = item[0] === '*' ? 'u' : 'o'
						let level = 0, j = uOrO === 'u' ? 0 : 1, name
						while (item[j] && item[j] !== ' ') {
							level ++
							j ++
						}
						name = convertInline(item.substring(level + (uOrO === 'u' ? 1 : 2)))
						return {uOrO, level, name}
					}
					function getGroup(lines) {
						// group is <(uo)l>items</(uo)l>
						let firstItem, style, lvl, elts, i, sg
						firstItem = itemInfos(lines[0])
						style = firstItem.uOrO
						lvl = firstItem.level
						elts = []
						i = 0
						while (i < lines.length) {
							let item = itemInfos(lines[i])
							if (item.level > lvl) {
								sg = getGroup(lines.slice(i))
								elts.push(sg[0])
								i += sg[1]
							} else if (item.level < lvl) {
								break
							} else {
								elts.push(`<li>${item.name}</li>`)
								i ++
							}
						}
						return [getHTML(style + 'l', elts.join('')), i]
					}
					return getGroup(lines)[0]
				}
				const getLines = (content, posDeb = 0) => { //retourne un tableau de lignes du bloc
					const lines = content.substring(posDeb).split('\n')
					let linesOk = []
					for (let l of lines) {
						l = l.trim()
						if (l !== '') {
							l = l.replace(/\s{2,}/, '\s')
							linesOk.push(l)
						}
					}
					return linesOk
				}
				content = this.getBlocks(content)
				let html = []
				for (let c of content) {
					// détection du type et création du bloc HTML en fonction
					if (c === '') {
						// si le contenu est vide, on a pas de bloc
					} else if (/[a-z]/i.test(c.substring(0,1))) {
						html.push(p(getLines(c)))
					} else if (/^[-_]{3,}$/.test(c)) {
						html.push('<hr>')
					} else if (c.substring(0,1) === '-') {
						html.push(discuss(getLines(c)))
					} else if (/^={1,6}/.test(c)) {
						let lvl = 0
						for (lvl; c[lvl] === '='; lvl ++) {
							if (lvl === 6) break
						}
						html.push(h(getLines(c, lvl), lvl))
					} else if (c.substring(0,1) === '>') {
						let caption = ''
						if (c.split('\n')[0].trim() !== '>') caption = getLines(c, 1)[0].trim()
						html.push(blockquote(getLines(c, 1), caption))
					} else if (c.substring(0,1) === '!') {
						let caption = ''
						if (c.split('\n')[0].trim() !== '!') caption = getLines(c, 1)[0].trim()
						html.push(figure(getLines(c, 1), caption))
					} else if (c.substring(0,1) === '#') {
						html.push(c.substr(1).trim())
					} else if (/^1?\*[^\*]/.test(c)) {
						html.push(li(getLines(c)))
					} else html.push(p(getLines(c)))
				}
				return html.join('')
			},
			getBlocks: function (content) { //content est un texte
				const replacer = '\|jumpline|/'
				return content.replace(/\n{2,}/g, replacer).split(replacer).map(c => c.trim())
			}
		}
	},
	cloneFile: file => {
		core.addFile(core.createFile({
			name: file.name + ' *',
			content: file.content
		}))
	},
	setLS: () => LS.setItem(LSname, JSON.stringify({dict: core.dict , files: core.files})),
	saveFile: (d, filename, type) => {
		const file = new Blob([d], {type})
		if (window.navigator.msSaveOrOpenBlob) // IE10+
			window.navigator.msSaveOrOpenBlob(file, filename)
		else { // Others
			const a = document.createElement('a')
			const url = URL.createObjectURL(file)
			a.href = url
			a.download = filename
			document.body.appendChild(a)
			a.click()
			setTimeout(function() {
				document.body.removeChild(a)
				window.URL.revokeObjectURL(url)
			}, 0)
		}
	}
}

// INIT
// 1. Si pas de clef LSname dans le LS, on la crée avec un tableau comportant un fichier
if (!LS.getItem(LSname)) {
	LS.setItem(
		LSname,
		JSON.stringify({
			dict: ['Mountale'],
			files: [
				core.createFile({
					name: 'À propos de Mountale',
					content: [
						`Ceci est la version ${version} de //Mountale//; une application d'écriture.`,
						`= Domaines d'utilisation`,
						`//Mountale// permet l'écriture de documents en tous genres. Cela peut aller de la simple prise de notes à l'écriture de romans en passant par la rédaction de publications de blog.`,
						`Pour cela, l'application intègre un correcteur orthographique, la possibilité de commenter des portions de texte, de rechercher et remplacer dans le texte. Il est possible d'exporter un document en HTML ou en ODT (//LibreOffice Writer//).`,
						'//Mountale// permet de regrouper des notes en un seul endroit. Celles-ci sont formatées en HTML grâce à un langage de balisage simple à prendre en main, ce qui leur donne une apparence claire et structurée. Il est facile d\'obtenir le HTML grâce à un bouton dédié "<>", ce qui permet de concevoir des publications de blog facilement.',
						'= Langage de balisage',
						'L\'édition du contenu se fait à l\'aide d\'un langage de balisage simple permettant d\'insérer des titres (de 1 à 6 \"=\"), des paragraphes, des listes (\"1*\" ou \"*\"), des sauts de ligne (\"\\-\\-\\-\"). Au sein du contenu, il est possible d\'insérer du **gras**, de l\'//italique//, un [[lien|https://diatomee.gitlab.io]], un indice,,x,,, un exposant^^x^^, du ``code``, un ||texte surligné||, un texte ++inséré++ ou --supprimé-- ou encore une image. Il suffit de cliquer sur ce texte pour afficher le mode d\'édition. En mode édition, les \"-\" indiqués pour le saut de ligne sont précédés d\'un antislash \"\\\\\". Cela permet d\'ignorer l\'effet du langage de balisage (ici, à cause du texte supprimé qui utilise les \"-\", sans les antislashes, une bonne partie du texte aurait été barrée. Pour afficher l\'antislash, il faut le doubler.',
						'! Dessin d\'un arbre.\n{{Texte alternatif|https://diatomee.gitlab.io/posts/2019/03/14/arbre700.jpg}}',
						'//Mountale// effectue un remplacement automatique :',
						'* du guillemet simple en apostrophe typographique;\n* de l\'espace simple en espace insécable ou espace fine insécable aux abords de la ponctuation;\n* des trois petits points en point de suspension.',
						'D\'autres fonctionnalités du langage de balisage seront implémentées ultérieurement (bloc de code, note de bas de page, tableau).',
						'---',
						'En attendant, bonne prise de notes :)'
					]
				})
			]
		})
	)
}
// 2. On récupère les fichiers du LS
const files = JSON.parse(LS.getItem(LSname)).files.reverse()
// 3. On fait de chaque fichier un objet "file"
for (let f of files) core.addFile(core.createFile(f))
// 4. On récupère le dictionnaire personnel
core.dict = JSON.parse(LS.getItem(LSname)).dict
