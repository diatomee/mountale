*Mountale* est une application permettant d'écrire. Elle fonctionne dans un navigateur.

Version alpha : *Moutale* en est à ses débuts et n'a pas encore (ou peu) été testé.

**Fonctionnalités :**

* correcteur orthographique (français)
* export en HTML
* export en ODT (pour modifier un document dans LibreOffice Writer, expérimental)
* rechercher / remplacer
* insertion de commentaires
* mise en forme aisée
* corrections typographiques et gestion des espaces (expérimental)

**Pour utiliser *Mountale* :**

* Télécharger et décompresser l'archive
* Placer le dossier où l'on veut dans l'ordinateur
* Ouvrir index.html dans un navigateur
* Enregistrer la page dans les marques pages pour un accés rapide

**Attention : La sauvegarde des données se fait dans le navigateur même grâce au "localStorage". Vider le stockage du navigateur supprimera toutes les notes créées. Il convient donc d'exporter fréquemment les notes sur un périphérique.**

*Mountale* nécessite l'apprentissage du *TaleML*, un langage de balisage (comme le *SkrivML* ou le *Markdown* par exemple) facile à prendre en main. La **documentation** se trouve dans un document par défaut, dans *Mountale* même (elle est encore incomplète, mais pratique quand même).
