- finir rechercher / remplacer, dans un composant
- l'éditeur devrait avoir une hauteur fixe et une barre de défilement
- rendre le code propre

- améliorer le mode "tale" :
	- tout marquage ou texte invisible apparait en gris, de taille normale
	- les commentaires sont surlignés
	- les titres sont grossis (sauf les "=")
	- et plus encore ...
	- code couleur : rouge = commentaire ou orthographe, bleu = lien, gris = non lisible

- on peut retirer les marques avec un simple bouton qui regarde où est le curseur

- quand on appuie sur Entrée on saute une ligne

- quand on appuie sur Maj + Entrée on a un retour à la ligne